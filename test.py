from group import *

def testExtend():
	a = Permutation([8, 1, 7, 3, 5, 2, 6, 4, 9, 0])
	b = Permutation([1, 0, 3, 2])
	aExt, bExt = a.extend(b)
	assert aExt == [8, 1, 7, 3, 5, 2, 6, 4, 9, 0] and bExt == [1, 0, 3, 2, 4, 5, 6, 7, 8, 9]

def testMulEven():
	a = Permutation([0, 1, 2, 4, 3])
	b = Permutation([4, 1, 2, 3, 0])

	c = a * b
	assert c == Permutation([3, 1, 2, 4, 0])

def testMulOdd():
	a = Permutation([5, 1, 6, 4, 2, 9, 0, 3, 7, 8])
	b = Permutation([3, 1, 0, 2])

	c = a * b
	assert c == Permutation([4, 1, 5, 6, 2, 9, 0, 3, 7, 8])

def testRefPerm():
	a = Permutation([6, 0, 2, 5, 4, 3, 1])
	b = Permutation([3, 5, 1, 2, 4, 6, 0])

	c = a * b
	print(c)
	assert c == Permutation([5, 3, 0, 2, 4, 1, 6])

def testEq():
	a = Permutation([1, 0, 2, 3, 4, 5])
	b = Permutation([1, 0, 2])

	assert a == b
	c = Permutation([1, 0, 3])
	assert a != c and b != c

