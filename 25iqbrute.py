from itertools import combinations, permutations
from collections import defaultdict
from functools import reduce
from operator import iconcat
from group import *
from tqdm import trange, tqdm
from math import comb
from sys import argv


def dumb(n):
	Id = list(range(n))
	IdPerm = Permutation(Id)
	Sn = list(map(Permutation, map(list, permutations(Id))))
	G = set()
	for elem in Sn:
		if elem == IdPerm:
			continue
		# print(elem, elem.inverse(), elem * elem.inverse())
		if elem == elem.inverse():
			pair = (elem,)
		else:
			pair = tuple(sorted((elem, elem.inverse())))
		if pair in G:
			continue
		G.add(pair)
	G = [x for x in set(G)]
	order = len(G)
	orderSn = len(Sn)
	potential_subgroups = defaultdict(list)
	def comb_improved(l: list[tuple], k: int):
		"""
		Finds combinations of size k where the sum 
		of the length of the tuples equals k
		"""
		def rec(vis: list, k: int, og_k: int, idx=0):
			if k == 0:
				# We love globals
				potential_subgroups[og_k].append(vis)
			for i in range(idx,len(l)):
				e = l[i]
				if len(e) > k or e in vis: continue
				rec(vis+[e],k-len(e),og_k,i+1)
		rec([],k,k)
	count = 1 # the identity group
	maxGroupSize = int(orderSn // 3)
	subgroups = []
	for size in range(1, maxGroupSize + 1):
		if orderSn % size != 0: continue
		comb_improved(G, size-1)
		for subgroup in potential_subgroups[size-1]:
			subset = reduce(iconcat, subgroup, []) + [IdPerm]
			SG = PermutationGroup(set(subset))
			count += SG.isGroup()
			if SG.isGroup(): subgroups.append(SG)
	return subgroups


if len(argv) == 2:
	n = int(argv[1])
else:
	print('Usage: ./10iqbrute.py N')
	quit()

def find(n: int, l: list[PermutationGroup]) -> list[PermutationGroup]:
	Id = list(range(n))
	IdPerm = Permutation(Id)
	Sn = list(map(Permutation, map(list, permutations(Id))))

	subs = set()
	for i in range(n-1):
		for sg in l:
			sub = []
			for e in sg:
				d = e.getDict()
				d[n-1] = d[i]
				for k,v in d.items():
					if v == i: d[k] = n-1
				d[i] = i
				sub.append(Permutation(list(d.values())))
			g = PermutationGroup(sub)
			assert g.isGroup()
			subs.add(g)
	for sg in l:
		sub = []
		for e in sg:
			sub.append(Permutation(e.perm + [n-1]))
		g = PermutationGroup(sub)
		assert g.isGroup()
		subs.add(g)
	print(len(subs))

	spans = []
	for f in combinations(Id, 2):
		rest = [i for i in range(5) if i not in f]
		for s in combinations(rest,3):
			spans.append(Permutation(conv_cycle([f,s])))
			#print(spans[-1])
			spans.append(Permutation(conv_cycle([f,tuple(list(s)[:1]+list(s)[1:][::-1])])))
	
	spans = spans[1:]
	for i in trange(1,len(spans)+1):
		for subgroup in combinations(spans, i):
			subset = set()
			for x in subgroup:
				subset |= x.spanGroup()
				for y in subgroup:
					if x == y: continue
					subset |= (x*y).spanGroup()

			SG = PermutationGroup(subset)
			if SG.isGroup():
				subs.add(SG)
	
	print(len(subs))


def conv_cycle(l: list[set[int]]) -> list[int]:
	d = {}
	for c in l:
		for i in range(len(c)):
			d[c[i]] = c[(i+1)%len(c)]
	#print(d)
	return list(d.values())
		


S4 = dumb(4)
find(5, S4)