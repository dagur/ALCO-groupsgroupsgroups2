from itertools import combinations, permutations
from functools import total_ordering
from math import lcm

# DEBUG = True
DEBUG = False

def dprint(*args, **kwargs):
	if DEBUG:
		print(*args, **kwargs)

@total_ordering
class Permutation:
	"""
	All permutations are zero indexed instead of one indexed
	"""
	def __init__(self, perm: list[int]) -> None:
		self.perm = perm
		self.order = None
	
	def getDict(self) -> dict:
		d = {}
		for i in range(len(self.perm)):
			d[i] = self.perm[i]
		return d

	def extend(self, other: 'Permutation') -> tuple[list[int], list[int]]:
		"""
		Takes 2 permutations and makes them equal length
		"""
		lenA, lenB = len(self), len(other)
		permA, permB = self.perm.copy(), other.perm.copy()

		if lenA > lenB:
			permB.extend(range(lenB, lenA))
		else:
			permA.extend(range(lenA, lenB))

		return permA, permB

	def order(self):
		""" Find order of the permutation """
		if self.order is not None: return self.order
		cycle_lens = [1] * len(self.perm)
		for i in self.perm:
			curr = self.perm[i]
			while curr != i:
				cycle_lens[i] += 1
				curr = self.perm[curr]

		self.order = lcm(*cycle_lens)
		return self.order
	
	def spanGroup(self) -> set['Permutation']:
		""" Find group that this permutation spans """
		seen = set()
		curr = Permutation(self.perm)
		curr = curr * self
		seen.add(curr)
		while curr != Permutation([0]):
			curr = curr*self
			seen.add(curr)

		return seen

	def __mul__(self, other: 'Permutation') -> 'Permutation':
		"""
		Right to left implementation of permutation multiplication.
		"""
		permA, permB = self.extend(other)
		res = [0] * len(permA)

		for i,v in enumerate(permB):
			res[i] = permA[permB[i]]

		return Permutation(res)

	def __eq__(self, other: object) -> bool:
		if not isinstance(other, Permutation):
			return False
		permA, permB = self.extend(other)
		return permA == permB

	def __len__(self) -> int:
		return len(self.perm)

	def __repr__(self) -> str:
		return str(self.perm)

	def __hash__(self):
		return hash(tuple(self.perm))

	def __ge__(self, other) -> bool:
		# Not correct in term of group theory
		# But rather correct in terms of making this script work
		return hash(self) > hash(other)

	def inverse(self):
		l = len(self.perm)
		perm = [0] * l
		for idx in range(l):
			perm[self.perm[idx]] = idx

		return Permutation(perm)



class PermutationGroup:
	# TODO: check if group is actually a group.
	def __init__(self, elements: set[Permutation]) -> None:
		self.elements = elements
		self.permLen = 0 if len(elements) == 0 else next(iter(elements))

	def __eq__(self, other: object) -> bool:
		if not isinstance(other, PermutationGroup):
			return False
		return self.elements == other.elements

	def __hash__(self):
		return hash(frozenset(self.elements))
		# PLEASE BE GOOD HASH PLEASE @DAGUR I REQUIRE THIS TO BE AMAZING 25IQ BRUTE DEPENDS ON THIS PLEASE

	def __repr__(self) -> str:
		return str(self.elements)

	def __iter__(self):
		for elem in self.elements:
			yield elem

	def isSubset(self, other):
		return other in self

	def isGroup(self):
		if self.permLen == 0:
			return False
		if Permutation(list(range(len(self.permLen)))) not in self.elements:
			dprint('Identity doesn\'t exist')
			return False
		for elemA in self.elements:
			if elemA.inverse() not in self.elements:
				dprint('Inverse doesn\'t exist')
				return False
			for elemB in self.elements:
				if elemA * elemB not in self.elements:
					dprint('Left closure doesn\'t exist')
					return False
				if elemB * elemA not in self.elements:
					dprint('Right closure doesn\'t exist')
					return False
		return True


def permutationSpan(perm: Permutation) -> PermutationGroup:
	# if perm * perm * perm == perm:
	# 	return PermutationGroup(set([perm, perm * perm]))
	span = set([perm])
	# starts at perm^2 because python doesn't have do while
	curr = perm * perm
	while curr != perm:
		span.add(curr)
		curr *= perm
	return PermutationGroup(span)

def mergeGroups(groupA: PermutationGroup, groupB: PermutationGroup):
	if groupA.isSubset(groupB):
		return groupA
	if groupB.isSubset(groupA):
		return groupB
	newSpan: set[Permutation] = set()
	for permA in groupA:
		for permB in groupB:
			newSpan.add(permA * permB)
			newSpan.add(permB * permA) # is this one necessary?
	return PermutationGroup(newSpan)

# print(permutationSpan(Permutation([0, 2, 3, 1])))
# a = Permutation([1,2,3,4,0])
# print(a.order())
