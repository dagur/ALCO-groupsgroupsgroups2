from itertools import combinations, permutations
from collections import defaultdict
from functools import reduce
from operator import iconcat
from group import *
from tqdm import trange, tqdm
from math import comb
from sys import argv


if len(argv) == 2:
	n = int(argv[1])
else:
	print('Usage: ./10iqbrute.py N')
	quit()

Id = list(range(n))
IdPerm = Permutation(Id)
Sn = list(map(Permutation, map(list, permutations(Id))))
G = set()
for elem in Sn:
	if elem == IdPerm:
		print('yes identity')
		continue
	# print(elem, elem.inverse(), elem * elem.inverse())
	if elem == elem.inverse():
		pair = (elem,)
	else:
		pair = tuple(sorted((elem, elem.inverse())))
	if pair in G:
		continue
	G.add(pair)

G = [x for x in set(G)]

order = len(G)
orderSn = len(Sn)

print(f'Sn: {orderSn}')
print(f'Gn: {order}')


potential_subgroups = defaultdict(list)
def comb_improved(l: list[tuple], k: int):
	"""
	Finds combinations of size k where the sum 
	of the length of the tuples equals k
	"""
	def rec(vis: list, k: int, og_k: int, idx=0):
		if k == 0:
			# We love globals
			potential_subgroups[og_k].append(vis)
		for i in range(idx,len(l)):
			e = l[i]
			if len(e) > k or e in vis: continue
			rec(vis+[e],k-len(e),og_k,i+1)
	rec([],k,k)

count = 1 # the identity group
maxGroupSize = int(orderSn // 3)



for size in range(1, maxGroupSize + 1):
	if orderSn % size != 0: continue
	print(f"Checking divisor {size}")
	comb_improved(G, size-1)
	print("Done generating")

	for subgroup in tqdm(potential_subgroups[size-1]):
		subset = reduce(iconcat, subgroup, []) + [IdPerm]

		SG = PermutationGroup(set(subset))
		count += SG.isGroup()

print(count)
