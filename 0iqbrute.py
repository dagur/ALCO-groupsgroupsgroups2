from itertools import combinations, permutations
from group import *
from tqdm import trange

# n = 5
n = 4
Id = list(range(n))
Sn = list(map(Permutation, map(list, permutations(Id))))
order = len(Sn)

count = 0
for size in trange(order + 1):
	for subgroup in combinations(Sn, size):
		SG = PermutationGroup(set(subgroup))
		# print(SG)
		count += SG.isGroup()
		# if SG.isGroup():
		# 	print(SG)

print(count)
