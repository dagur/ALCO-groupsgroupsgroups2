from itertools import combinations, permutations
from functools import reduce
from operator import iconcat
from group import *
from tqdm import trange, tqdm
from math import comb
from sys import argv

# not correct but may be useful # TODO: either generate inverses later or generate order 3 elements later or maybe calculate some correction factor
# TODO: Þetta er actuallt order 2 elements því (ord2Elem, ord2Elem^{-1}) = (ord2Elem, ord2Elem)

# n = 5
if len(argv) == 2:
	n = int(argv[1])
else:
	print('Usage: ./10iqbrute.py N')
# n = 4
Id = list(range(n))
IdPerm = Permutation(Id)
Sn = list(map(Permutation, map(list, permutations(Id))))
G = set()
for elem in Sn:
	if elem == IdPerm:
		print('yes identity')
		continue
	# print(elem, elem.inverse(), elem * elem.inverse())
	if elem == elem.inverse():
		pair = (elem,)
	else:
		pair = tuple(sorted((elem, elem.inverse())))
	if pair in G:
		continue
	G.add(pair)

G = [x for x in set(G)]

order = len(G)
orderSn = len(Sn)

print(f'Sn: {orderSn}')
print(f'Gn: {order}')

count = 1 # the identity group
maxGroupSize = int(orderSn // 3)
if maxGroupSize < order:
	count += 1
else:
	maxGroupSize = order

estComplexity = maxGroupSize + 2 * maxGroupSize.bit_length()

print(f'Est. Complexity: 2^{estComplexity}')

for size in range(1, maxGroupSize + 1):
# for size in trange(1, maxGroupSize + 1):
	# if orderSn % size
	# for subgroup in combinations(G, size):
	nchoosek = comb(order, size)
	# print(nchoosek)
	print(f'subgroups found: {count}')
	for subgroup in tqdm(combinations(G, size), total = nchoosek):
	# for subgroup in tqdm(combinations(G, size)):
		subset = reduce(iconcat, subgroup, []) + [IdPerm]
		if orderSn % len(subset) != 0:
			continue
		SG = PermutationGroup(set(subset))
		# if orderSn % len(SG.elements) != 0: # very not ideal
		# 	continue
		# valid = SG.isGroup()
		# if valid:
		# 	print(size, len(subset), len(set(subset)))
		# count += valid
		count += SG.isGroup()
		# if SG.isGroup():
		# 	print(SG)

print(count)
